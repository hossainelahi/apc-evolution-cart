package pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import DataSource.ConfigReader;
import tools.Driver;

public class APCBreadCrumb {
	public static String MainManuXPath = "//*[@class='main-menu']/li[2]/ul/li";
	public static By MainManu = By.xpath(MainManuXPath);
	/*main manu is devided into to parts. one for mobile one for desktop.
	 1st item is my account which may not accesable to some country. 2nd item is mobile version of My account
	 total main menu item2 = (mainmenu.size()-2)/2   ------------------------*/
	

	
	public static void testBreadCrumb(WebDriver driver){
		String baseURL = ConfigReader.getPropValues("baseURL");
		driver.get(baseURL);
		Driver.waitToLoad(driver, "APC");
		Driver.wait(5000);
		List<WebElement> MainMenuElements = driver.findElements(MainManu);
		if(MainMenuElements.size()>0){
			MainMenuElements.get(3).click();
			Driver.wait(5000);
			String SubMenuXpath = MainManuXPath+"[4]/div/div/ul/li";
			List<WebElement> SubMenuElements = driver.findElements(By.xpath(SubMenuXpath));
			if(!(SubMenuElements.isEmpty())){
				SubMenuElements.get(0).click();
				Driver.wait(5000);
				driver.findElement(By.xpath(SubMenuXpath+"[1]"+"/div/div/ul/li"+"[1]"+"/a")).click();
				Driver.waitToLoad(driver, "home");
				Driver.wait(8000);
				int number = driver.findElements(By.xpath("//*[@id='trail']/ul/li")).size();
				System.out.println(number + " is the number of trail exist in this page. URL: "+ driver.getCurrentUrl());
			}
			
			
			
		}
		
	}
}
