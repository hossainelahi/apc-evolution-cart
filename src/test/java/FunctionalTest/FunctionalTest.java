package FunctionalTest;

import org.testng.annotations.Test;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import DataSource.ConfigReader;
import pages.HomeAPC;
import pages.APCBreadCrumb;
import tools.Driver;

import DataSource.Xls_Reader;

public class FunctionalTest {
	
	public static WebDriver driver;
	
	@BeforeSuite(description = "Initiate the test")
	public static void initiate(){
			driver = Driver.initialize(driver);
			String url = (String)ConfigReader.getPropValues("baseURL");
			driver.get(url);
			Driver.waitToLoad(driver, "APC");
			System.out.println(Driver.logTime()+ "  "+url + " loded");
		}
	
	@Test (dataProvider="loginData", description = "Login to the E-Commerce site.")
	public static void login(String user, String pass, String name){
		System.out.println("Login Process initiated");
		boolean valid = HomeAPC.loginValid(driver, user, pass, name);
		Assert.assertTrue(valid, Driver.logTime()+" ...... login Successfull for username: "+ user);
	}
	
	@Test (dependsOnMethods="login", description = "Check the value of cart")
	public static void cartValue(){
		String url = driver.getCurrentUrl();
		String title = driver.getTitle();
		String cartValueNEW = HomeAPC.cartValueNew(driver);
		driver.get("http://uat.apc.com/shop/us/en/myaccount/dashboard.jsp");
		Driver.waitToLoad(driver, "My Account");
		String cartValueOLD = HomeAPC.cartValueOld(driver);
		driver.get(url);
		Driver.waitToLoad(driver, title);
		boolean condition = cartValueNEW.contains(cartValueOLD) && !cartValueOLD.isEmpty();
		Assert.assertTrue(condition, "Cart Value in both platform are same");
	}
	
	
	@Test
	public static void validateBreadCrumb(){
		String oldurl = driver.getCurrentUrl();
		String oldTitle = driver.getTitle();
		APCBreadCrumb.testBreadCrumb(driver);
		
		driver.get(oldurl);
		Driver.waitToLoad(driver, oldTitle);
		
	}
	
	@Test (dataProvider="keyCodeRead")
	public static void keycode(String key, String validity){
		Driver.wait(1000);
		boolean valid = true;
		if (validity.contains("invalid"))
			valid = false;
		boolean keyCodeTest = HomeAPC.testKey(driver, key, valid);
		Assert.assertEquals(keyCodeTest, true, Driver.logTime()+" ........ KeyCode Test Passed");
	}
	
	
	
	@AfterSuite
	public static void close(){
		HomeAPC.logout(driver);
		Driver.close(driver);
	}
	
	

	@DataProvider(name="BreadCrumbData")
	public static Object[][] readBreadCrumb(){
		Object[][] result = new Object[4][2];
		
		return result;
	}
	
	
	
	
	@DataProvider(name="loginData")
	public static Object[][] readLoginData(){
		Object [][] data = new Object[1][3];
		data[0][0] = (String)ConfigReader.getPropValues("user");
		data[0][1]= (String)ConfigReader.getPropValues("pass");
		data[0][2] = (String)ConfigReader.getPropValues("name");
		System.out.println(data[0][0]+"  "+data[0][1] +"  "+data[0][2]);
		return data;
	}
	
	@DataProvider(name= "keyCodeRead")
	public static Object[][] readKeyCode(){
		// read data from xml not from properties file
		String path = Xls_Reader.filename;
		String sheetName = "KeyCode";
		Xls_Reader file = new Xls_Reader(path);
		int row = file.getRowCount(sheetName);
		Object[][] key = new Object[row-1][2];
		for (int i=0; i<row-1; i++){
			key [i][0] = file.getCellData(sheetName, 0, i+2);
			key [i][1] = file.getCellData(sheetName, 1, i+2);
		}
		return key;
	}

}
