package DataSource;

import FunctionalTest.FunctionalTest;
import DataSource.ConfigReader;

public class xlsData {
	
	public static void main (String[] arg){
		readData(1);
		
	}
	
	
	public static String readData(int i){
		String data;
		String path = Xls_Reader.filename;
		String sheetName = "CountryName";
		Xls_Reader file = new Xls_Reader(path);
		int row = file.getRowCount(sheetName);
		data = file.getCellData(sheetName, 1, i);
		System.out.println(data + "    row count: "+ row);
		return data;
	}

}
