package DataSource;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import java.util.Properties;

import tools.Driver;
 
/**
 * @author Hossain
 * 
 */
 
public class ConfigReader {
	
	public static void main(String[] arg){
		String data = getPropValues("countryselector");
		System.out.println(data);
		
	}
 

	public static String getPropValues(String propName) {
		String value = null;
		FileInputStream inputStream;
 
		try {
			Properties prop = new Properties();
			String propFileName = System.getProperty("user.dir")+"\\src\\test\\java\\DataSource\\APC.properties";
 
			inputStream = new FileInputStream(propFileName);
 
			prop.load(inputStream);
 
//			Date time = new Date(System.currentTimeMillis());
 
			// get the property value and print it out
			value = prop.getProperty(propName);

			System.out.println( propName+" : "+Driver.logTime()+", "+value + " is been read.");
			inputStream.close();
			

		} catch (Exception e) {
			System.out.println("Exception: " + e);
		}
		return value;
	}
}

