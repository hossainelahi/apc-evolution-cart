package tools;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.FluentWait;
import org.testng.Reporter;

import com.google.common.base.Function;

public class Driver {

	public static WebDriver initialize(WebDriver driver){
		System.setProperty("webdriver.chrome.driver", "src\\test\\java\\dataSource\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.get("http://www.google.com");
		driver.manage().window().maximize();
		boolean code = false;
		while (!code){
			String title = driver.getTitle();
			code = title.contains("Google");
		}
		return driver;
	}
	
	public static void waitToLoad(WebDriver driver, String title){
		boolean status = false;
		
		while(status){
			String curTitle = driver.getTitle();
			status = curTitle.contains(title);
		}
//		return status;
	}
	
	public static void click(WebDriver driver, String locator, String location )
	{
		try{
			By by = getBy(locator,location);
			WebElement webElement = driver.findElement(by);
			if (webElement != null){
				webElement.click();
			}
		}catch (ElementNotVisibleException e) {
			throw e;
		} catch (WebDriverException e) {
			if (e.getMessage().startsWith(
					"unknown error: " + "Element is not clickable at point")) {
			}
		} catch (Exception e) {
			Reporter.log("Message is : " + e.getMessage(), true);
			e.printStackTrace();
		}
		
		
	}
	
	public static By getBy(String locator, String location) {
		By by = null;
		if (locator.equalsIgnoreCase("id")){
			by = By.id(location);
		}
		else if (locator.equalsIgnoreCase("xPath")){
			by = By.xpath(location);
		}
		else if (locator.equalsIgnoreCase("linkText")){
			by = By.linkText(location);
		}
		else if (locator.equalsIgnoreCase("cssSelector")){
			by = By.cssSelector(location);
		}
		else if (locator.equalsIgnoreCase("name")){
			by = By.name(location);
		}
		return by;
	}
	
	public static WebElement findElement(final WebDriver driver,
			final By locator, final int timeoutSeconds) {
		FluentWait<WebDriver> wait = new FluentWait<WebDriver>(driver)
				.withTimeout(timeoutSeconds, TimeUnit.SECONDS)
				.pollingEvery(300, TimeUnit.MILLISECONDS)
				.ignoring(NoSuchElementException.class);

		return wait.until(new Function<WebDriver, WebElement>() {
			public WebElement apply(WebDriver webDriver) {
				return driver.findElement(locator);
			}
		});
	}
	
	public static WebElement findElement(WebDriver driver, String locator,
			String location, int timeoutSeconds) {
		By by = getBy(locator, location);
		return findElement(driver, by, timeoutSeconds);
	}
	
	public static void wait(int milisec){
		try {
			Thread.sleep(milisec);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static String logTime() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		return dateFormat.format(cal.getTime());
	}
	
	public static void close(WebDriver driver){
		driver.close();
		driver.quit();
	}
}
