package pages;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import tools.Driver;

public class HomeAPC {
	public static By keycode = By.xpath("//footer/ul[2]/li[4]/form/div/input");
	public static By APClogo = By.xpath("//*[@class='main-menu']/li/a/img");
	public static By MyAccNew = By.xpath("//*[@id='multi-login-container']/div");
	public static By LogInNew = By.id("login-button");
	public static By MyAccOld = By.partialLinkText("My Account");
	public static By SignInOld = By.xpath("//*[@id='header-links']/div[1]/div[3]/div[1]/a");
	public static By CartCountOld = By.id("cart-count");
	public static By CartCountNew = By.id("cart-button");
	public static By email = By.id("txtEmailAddress");
	public static By password = By.id("txtPassword");
	public static By signIn = By.xpath("//*[@id='form-sign-in']/div[4]/div[2]/input");
	public static By loginerrormsg = By.id("txtPassword-error");
	public static By LogInDashBoard = By.id("ma-dashboard");
	public static By MyAccount = By.id("user-name");
	public static By logoutNEW = By.id("logout-button");
//	public static By CusSignIn = By.xpath("//*[@id='header-links']/div[1]/div[3]/div[1]/a");


	
	public static boolean loginValid(WebDriver driver, String user, String pass, String name){
		boolean status = false;
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		String url = driver.getCurrentUrl();
		driver.findElement(MyAccNew).click();
		driver.findElement(LogInNew).click();
		Driver.waitToLoad(driver, "OpenAM");
		Driver.wait(1000);
		driver.findElement(email).sendKeys(user);
		driver.findElement(password).sendKeys(pass);
		driver.findElement(signIn).click();
		Driver.waitToLoad(driver, "MyAccount");
		String LoginMsg = driver.findElement(LogInDashBoard).getText();
		if (LoginMsg.contains(name)){
			System.out.println("Login Successfull");
			status = true;
		}
		driver.get(url);
		Driver.waitToLoad(driver, "APC");
		return status;
	}
	
	public static void logout(WebDriver driver){
		try {
			WebElement myAccount = driver.findElement(MyAccount);
			if (myAccount.getText().contains("Hello")){
				myAccount.click();
				driver.findElement(logoutNEW).click();
				wait(1000);
				System.out.println(Driver.logTime()+ "  ........ Sytem Logout successfull.");
			}
			
		}catch(NoSuchElementException e){
			System.out.println(Driver.logTime()+ "  ....... System is not loged in. So system cant logout.");
		}
	}
	
	
	
	
	public static void loginValidOld(WebDriver driver, String user, String pass){
		boolean status;
		String url = driver.getCurrentUrl();
		if (url.contains("/us/en")){
			Actions mouse = new Actions(driver);
			mouse.moveToElement(driver.findElement(MyAccOld));
			mouse.perform();
			wait(1000);
//			driver.findElement(CusSignIn).click();
			driver.findElement(SignInOld).click();
			Driver.waitToLoad(driver, "(Login)");
			driver.findElement(email).sendKeys(user);
			driver.findElement(password).sendKeys(pass);
			driver.findElement(signIn).click();
			Driver.wait(100);
			List<WebElement> errormasg = driver.findElements(loginerrormsg);
			if(!errormasg.isEmpty()&&errormasg.get(0).isDisplayed()){
				Driver.waitToLoad(driver, "My Account");
//				login successfull
				
			}else{
				
			}
		}
		else{
			System.out.println("Link is not belongs to USA. Login Test Abort.");
		}
	}
	
	public static String cartValueOld(WebDriver driver){
		String count = driver.findElement(CartCountOld).getText();
		count= count.substring(1, count.length()-1);
		System.out.println(count +" .... is count value in old.");
		return count;
	}
	public static String cartValueNew(WebDriver driver){
		String count = driver.findElement(CartCountNew).getAttribute("value");
		System.out.println(count+ "   ... is count value in new.");
		return count;
	}

	public static boolean testKey(WebDriver driver, String key, boolean valid){
		boolean status = false;
		WebElement KeyCode = driver.findElement(keycode);
		KeyCode.clear();
		KeyCode.sendKeys(key);
		KeyCode.sendKeys(Keys.ENTER);
		String ErrorMassage = "We were unable to find a promotion associated with the keycode you provided";
		//switch tab
		Driver.wait(100);
			ArrayList<String> window = new ArrayList<String>(driver.getWindowHandles());
			if (window.size()>1){
				driver.switchTo().window(window.get(window.size()-1));
			}else{
				System.out.print(Driver.logTime()+" ...... No new tab opend.");
				if (!valid){
					System.out.println(" As expected. Test case passed.");
					status = true;
					return status;
				}else{
					System.out.println(" Not expected. Test case faild for this key code: " + key);
					status = false;
					return status;
				}
			}
//				System.out.println("Number of window is : "+ window.size());

				Driver.waitToLoad(driver, "Promotion");
				boolean msgNotVis = !(driver.findElement(By.tagName("body")).getText().contains(ErrorMassage));
				if (msgNotVis && valid){
					status = true;
					System.out.println(Driver.logTime()+ " ..... Keycode passad. As expected. Test case passed");
				}else if ( !msgNotVis && !valid){
					status = true;
					System.out.println(Driver.logTime()+ " ..... Keycode faild. As expected. Test case passed.");
				}
				else {
					status = false;
					System.out.println(Driver.logTime()+ " ..... Keycode faild. Not expected. Test case faild for this keycode: "+ key);
				}
				ArrayList<String> windowNew = new ArrayList<String>(driver.getWindowHandles());
				if(windowNew.size()>1){
					driver.switchTo().window(windowNew.get(windowNew.size()-1));
					driver.close();
					driver.switchTo().window(windowNew.get(windowNew.size()-2));
				}
			/*}
			else{
				status = false;
				System.out.println(Driver.logTime()+ " ..... No new tab opened after inputting keycode. Inspect error: HomeAPC: 53");
			}*/	
		return status;
	}
	
	
	public static void wait(int milisec){
		try {
			Thread.sleep(milisec);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
